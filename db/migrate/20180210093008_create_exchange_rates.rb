class CreateExchangeRates < ActiveRecord::Migration[5.1]
  def change
    create_table :exchange_rates do |t|
      t.date :period
      t.float :unit
    end
    add_index :exchange_rates, :period, unique: true
  end
end
