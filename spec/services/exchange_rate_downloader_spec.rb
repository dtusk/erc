require 'rails_helper'

RSpec.describe ExchangeRateDownloader do
  describe '#download_and_parse_data_source' do
    let(:download_and_parse_data_source) { subject.download_and_parse_data_source }

    it 'returns proper values' do
      VCR.use_cassette('exchange_rate_downloader__download_and_parse_data_source') do
        expect(download_and_parse_data_source).to be_a(Array)
        expect(download_and_parse_data_source.length).to be >= 0
      end
    end

    
    it 'contains period and unit keys' do
      VCR.use_cassette('exchange_rate_downloader__download_and_parse_data_source') do
        download_and_parse_data_source.each do |data|
          expect(data[:period]).not_to be nil
          expect(data[:unit]).not_to be nil
          expect(data.keys.length).to eq 2
        end
      end
    end

    context 'when EOFError' do
      before do
        allow(subject).to receive(:open).and_raise(EOFError)
      end

      it 'stores a message in instance variable' do
        expect(subject.errors).to be_a(Array)
        expect(subject.download_and_parse_data_source).to be_falsey
        expect(subject.errors).not_to be nil
        expect(subject.errors?).to be_truthy
        expect(subject.errors).to eq(['Headers not found. Check data source if it is not changed'])
      end
    end

    context 'when SocketError' do
      before do
        allow(subject).to receive(:open).and_raise(SocketError)
      end

      it 'stores a message in instance variable' do
        expect(subject.errors).to be_a(Array)
        expect(subject.download_and_parse_data_source).to be_falsey
        expect(subject.errors).not_to be nil
        expect(subject.errors?).to be_truthy
        expect(subject.errors).to eq(['Internet connection'])
      end
    end
  end

  describe '#seed' do
    let(:exchange_rate_count) { ExchangeRate.count }
    let(:data_source) { subject.download_and_parse_data_source }

    it 'stores the data from API' do
      VCR.use_cassette('exchange_rate_downloader__download_and_parse_data_source_small') do
        expect { subject.seed(data_source) }.to change { ExchangeRate.count }.from(0).to(10)
      end
    end
  end
end
