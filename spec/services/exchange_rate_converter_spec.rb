require 'rails_helper'

RSpec.describe ExchangeRateConverter do

  let!(:rate) { build_stubbed(:exchange_rate, period: '2018-02-09', unit: 1.2273) }
  
  before do
    allow(ExchangeRate).to receive(:retrieve_period).and_return(rate)
  end

  subject!(:converter) { described_class }

  describe '#convert' do
    it { expect(converter.convert(1, date: '2018-02-09')).to eq(0.8147967082212988) }
    it { expect { converter.convert(1, date: '2018-02-30') }.to raise_error(message = 'Date is not valid') }
    it { expect { converter.convert('foo', date: '2018-02-02') }.to raise_error(message = 'Value is not valid') }
  end
end
