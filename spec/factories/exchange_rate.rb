FactoryBot.define do
  factory :exchange_rate do
    period { '2018-02-09' }
    unit { 1.2273 }
  end
end
  