require 'rails_helper'

RSpec.describe ExchangeRate, type: :model do
  context 'validations' do
    it { is_expected.to validate_presence_of(:period) }
    it { is_expected.to have_db_index(:period) }
    it { is_expected.to validate_uniqueness_of(:period) }
    it { is_expected.to validate_presence_of(:unit) }
    it { is_expected.to validate_numericality_of(:unit) }
  end
end
