require 'rails_helper'

RSpec.describe DateValidator do
  context 'valid validatons' do
    it { expect(described_class.validate('1970-01-01')).to be_truthy }
    it { expect(described_class.validate('01-01-1970')).to be_truthy }
  end

  context 'invalid validations' do
    it { expect(described_class.validate('foo')).to be_falsey }
    it { expect(described_class.validate('1970-123-01')).to be_falsey }
    it { expect(described_class.validate('1970-01-213')).to be_falsey }
  end
end
