require 'rails_helper'

RSpec.describe NumberValidator do
  context 'valid validatons' do
    it { expect(described_class.validate(1)).to be_truthy }
    it { expect(described_class.validate(-1.0)).to be_truthy }
  end

  context 'invalid validations' do
    it { expect(described_class.validate('foo')).to be_falsey }
    it { expect(described_class.validate('0.foo123')).to be_falsey }
  end
end
