class DateValidator
  class << self
    # Check if date is valid
    #
    # @param value [String]
    #
    # @return [Boolean]
    def validate(value)
      Date.parse(value) && (value <= Date.today.to_s)
    rescue ArgumentError
      false
    end
  end
end
