class NumberValidator
  class << self
    # Check if number is a number
    #
    # @param number [String]
    #
    # @return [Boolean]
    def validate(number)
      Float(number).present?
    rescue StandardError
      false
    end
  end
end
