require 'activerecord-import'

class ExchangeRate < ApplicationRecord
  validates :unit, presence: true, numericality: true
  validates :period, presence: true, uniqueness: true

  # Retrieve the latest value for the given period
  scope :retrieve_period, ->(curr_period) { order(period: :desc).where('period <= ?', curr_period).first }
end
