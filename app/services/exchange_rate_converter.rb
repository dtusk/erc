class ExchangeRateConverter

  private_class_method :new

  # Initializes and validates the provided data
  #
  # @param value [Float]
  # @param date [String] in date format
  def initialize(value, date)
    raise 'Value is not valid' unless NumberValidator.validate(value)
    raise 'Date is not valid' unless DateValidator.validate(date)
    @value = value
    @date = date
  end

  # Proxifies to corresponding instance method
  #
  # @param value [Float]
  # @param date [String] in date format
  def self.convert(value, date:)
    new(value, date).convert
  end

  # Convert given value with the given date in #initialize and
  # calculate the exchange rate from USD in EUR and returns the value.
  #
  # @return [Float] value from the exchange rate from USD in EUR
  def convert
    unit = ExchangeRate.select(:unit).retrieve_period(@date).unit

    @value / unit
  end
end
