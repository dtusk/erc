require 'open-uri'
require 'csv'

class ExchangeRateDownloader
  ECB_DATA_SOURCE = 'http://sdw.ecb.europa.eu/quickviewexport.do?SERIES_KEY=120.EXR.D.USD.EUR.SP00.A&type=csv'.freeze

  attr_reader :errors

  # Intialize an empty array of errors
  def initialize
    @errors = []
  end

  # Check if any errors are present
  #
  # @return [Boolean]
  def errors?
    @errors.present?
  end

  # Download and parse the result of ECB to significant data.
  #
  # @return [Array<Hash>] of results
  # @return [Boolean] if any errors occured
  def download_and_parse_data_source
    headers = "Period\\Unit:,\[US dollar\ \]\r\n"

    open(ECB_DATA_SOURCE) do |file|
      while file.readline != headers do; end
      csv_data = "period,unit\r\n"
      csv_data << file.read
      parse_csv(csv_data)
    end
  rescue EOFError
    @errors << 'Headers not found. Check data source if it is not changed'
    false
  rescue SocketError
    @errors << 'Internet connection'
    false
  end

  # Seed our database with data
  def seed(data)
    ExchangeRate.import(data, on_duplicate_key_ignore: true)
  end

  private

  # Parse CSV with our data
  #
  # @param csv_data [String]
  # @param results [Array<Hash>]
  def parse_csv(csv_data)
    [].tap do |results|
      CSV.parse(csv_data, headers: true) do |csv|
        if DateValidator.validate(csv['period']) && NumberValidator.validate(csv['unit'])
          results << { period: csv['period'], unit: csv['unit'] }
        end
      end
    end
  end
end
