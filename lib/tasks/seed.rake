namespace :exchange_rate_converter do
  desc 'Seed the database with ECB data'
  task seed: [:environment] do
    exchange_rate_downloader = ExchangeRateDownloader.new
    data = exchange_rate_downloader.download_and_parse_data_source
    abort(exchange_rate_downloader.errors.join) if exchange_rate_downloader.errors?
    exchange_rate_downloader.seed(data)
  end
end
