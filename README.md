# ERC tasks

This application uses Ruby 2.5.0 with PostgreSQL as database.

## Database

Make sure you have PostgreSQL installed with development files,
in order to install `pg` gem. Every version of PostgreSQL should work flawlessly.
Create the database with `bundle exec rails db:setup`

## Run tests

You can run test simply with

```sh
   bundle exec rspec
```

## Seed

Run the rake task `exchange_rate_converter:seed`

```bash
bundle exec rake exchange_rate_converter:seed
```

## Convert rates

Run the rails console `bundle exec rails c` and use the `ExchangeRateConverter` class.

### Examples

```ruby
ExchangeRateConverter.convert(100, date: '2018-01-01')
```